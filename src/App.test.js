import React from 'react';
import enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import App from './App';

enzyme.configure({ adapter: new Adapter.default() });

 it('renders without crashing', () => {
  enzyme.shallow(<App />);
 });
